<?php
namespace App\Gender;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Gender extends DB
{
    public $id;
    public $name;
    public $sex;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name',$postVariableData)) {
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists('sex',$postVariableData)) {
            $this->sex = $postVariableData['sex'];
        }
    }

    public function store(){

        $arrData = array($this->name, $this->sex);
        $sql = "Insert INTO gender(name, sex) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method

}



