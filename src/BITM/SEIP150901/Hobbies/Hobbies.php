<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Hobbies extends DB{
    public $id;
    public $name	;
    public $hobbies;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name',$postVariableData)) {
            $this->name = $postVariableData['name'];
        }
        if (array_key_exists('hobbies',$postVariableData)) {
            $arr= $postVariableData['hobbies'];

            $this->hobbies = implode(",",$arr);
        }
    }

    public function store(){

        $arrData = array($this->name, $this->hobbies);
        $sql = "Insert INTO hobbies(name, hobbies) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method

}



