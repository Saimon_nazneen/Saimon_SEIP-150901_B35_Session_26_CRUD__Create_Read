<?php
namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB{
    public $id;
    public $person_name	;
    public $birthdate;
    public $date_format;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('person_name',$postVariableData)) {
            $this->person_name = $postVariableData['person_name'];
        }
        if (array_key_exists('birthdate',$postVariableData)) {
            $this->birthdate = $postVariableData['birthdate'];
        }
        if (array_key_exists('date_format',$postVariableData)) {
            $this->date_format = $postVariableData['date_format'];
        }
    }

    public function store(){

        $arrData = array($this->person_name, $this->birthdate);
        //var_dump ($arrData);
        $sql = "Insert INTO birthday(person_name, birthdate) VALUES (?,?)";

        //var_dump($sql);
        //die;

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method

}



