<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>
<html>
<head>
    <title>Hobbies</title>
    <link rel="stylesheet" href="../../../Resource/assets_hobbies/css/style.css" />
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <div class="main">
        <h2>Hobbies</h2>
        <form role="form" action="store.php" method="post" class="login-form">
            <div class="form-group"><i class="fa fa-user"></i>
                <label>Username :</label>
                <input type="text" name="name" id="username">
            </div>
            <div class="form-group"><i class="fa fa-check"></i>
            <label class="heading">Select Your Hobbies</label>
            <div class="form-group">
                <input type="checkbox" name="hobbies[]" value="travelling"><label>Travelling</label></div>
             <div class="form-group">
                <input type="checkbox" name="hobbies[]" value="progamming"><label>Programming</label></div>
             <div class="form-group">
                <input type="checkbox" name="hobbies[]" value="gardening"><label>Gardening</label></div>
             <div class="form-group">
                 <input type="checkbox" name="hobbies[]" value="working"><label>Working</label></div>
             <div class="form-group">
                 <input type="checkbox" name="hobbies[]" value="reading"><label>Reading Book</label></div>
             <div class="form-group">
                 <button type="submit" class="btn">Create</button>
                </div>
            <!----- Including PHP Script ----->

        </form>
    </div>
</div>
</body>
</html>
<!--

if(isset($_POST['submit'])){
    if(!empty($_POST['check_list'])) {
// Counting number of checked checkboxes.
        $checked_count = count($_POST['check_list']);
        echo "You have selected following ".$checked_count." option(s): <br/>";
// Loop to store and display values of individual checked checkbox.
        foreach($_POST['check_list'] as $selected) {
            echo "<p>".$selected ."</p>";
        }
        echo "<br/><b>Note :</b> <span>Similarily, You Can Also Perform CRUD Operations using These Selected Values.</span>";
    }
    else{
        echo "<b>Please Select Atleast One Option.</b>";
    }
}
?>
-->