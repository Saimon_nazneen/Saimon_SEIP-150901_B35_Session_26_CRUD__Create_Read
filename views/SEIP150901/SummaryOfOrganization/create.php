<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Company Details</title>
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_sum/css/form_value.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include JS File Here -->
    <script src="../../../Resource/assets_sum/js/jquery.min.js"></script>
    <script type="text/javascript" src="../../../Resource/assets_sum/js/form_value.js"></script>
</head>
<body>
<div class="container">
    <h2>Summary of Organization</h2>
    <div class="main">

        <form action="store.php" method="post">
            <!-- Text -->
            <div class="form-group">
            <label>Company name:</label>
            <input type="text" id="text" name="org_name" value="" />
            </div>
            <br>

            <!-- Textarea -->
            <div class="form-group"> <i class="fa fa-"></i>
            <label>Company Summary:</label>
            <textarea id="textarea" name="org_summary"></textarea>
            </div>
            <br>
            <button type="submit" class="btn">Create</button>
        </form>
    </div>
</div>
</body>
</html>